class PlayersController < ActionController::API
  # I S N E C U D - index, show, new, edit, create, update, destroy
  # C R U D - create, read, update, destroy
  def index
    render json: Player.all
  end

  def create
    player = Player.new(player_params)

    if player.save
      render json: player.attributes
    else
      render json: { success: false, message: 'Failed to create player' }
    end
  end

  def update
    player = Player.find(params[:id])

    if player.update(player_params)
      render json: player.attributes
    else
      render json: { success: false, message: 'Failed to update player with id ' + params[:id] }
    end
  end

  def destroy
    player = Player.find(params[:id])

    if player.destroy
      render json: player.attributes
    else
      render json: { success: false, message: 'Failed to destroy player by id ' + params[:id] }
    end
  end

  private

  def player_params
    params.require(:player).permit(:name, :wins)
  end
end